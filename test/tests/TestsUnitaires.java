package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import modele.metier.Representation;
import org.junit.*;
import static org.junit.Assert.*;
import vue.VueListeRepresentations;
import modele.metier.Representation;

/**
 *
 * @author nragazzi
 */
public class TestsUnitaires {
     private List<Representation> lesRepresentations;
     private Representation uneRepresentation;
     
    @Test
    public void testListeRepresentations () {
      EntityManager em;
      em = Persistence.createEntityManagerFactory("FestivalBilleteriePU").createEntityManager();
       try{
        Query query = em.createQuery("select r from Representation r");
        lesRepresentations = (List<Representation>) query.getResultList();
      }catch (Exception ex) {
	System.out.println("erreur de récupération : "+ex.getMessage());
      }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat tf = new SimpleDateFormat("HH.mm.ss");
        System.out.println("LISTE DES REPRESENTATIONS : ");
       for (Representation uneRepresentation : lesRepresentations) {
           String id = uneRepresentation.getId();    
           String maDate = df.format(uneRepresentation.getJour());           
           String debut = tf.format(uneRepresentation.getDebut());
           String fin = tf.format(uneRepresentation.getFin());
           String nomGroupe = uneRepresentation.getGroupe().getNom();
           String nomLieu = uneRepresentation.getLieu().getNom();
           String nbPlacesVendues = String.valueOf(uneRepresentation.getPlacesVendues());
	
            System.out.println(id+" - "+maDate+" - "+debut+" - "+fin+" - "+nomGroupe+" - "+nomLieu+" - "+nbPlacesVendues);
        }

    }
    @Test
    public void testUneRepresentations () {
        EntityManager em;
      em = Persistence.createEntityManagerFactory("FestivalBilleteriePU").createEntityManager();
       try{
        Query query = em.createQuery("select r from Representation r where r.id = 2 ");
        uneRepresentation = (Representation) query.getSingleResult();
      }catch (Exception ex) {
	System.out.println("erreur de récupération : "+ex.getMessage());
      }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat tf = new SimpleDateFormat("HH.mm.ss");

        String id = uneRepresentation.getId();    
        String maDate = df.format(uneRepresentation.getJour());           
        String debut = tf.format(uneRepresentation.getDebut());
        String fin = tf.format(uneRepresentation.getFin());         String nomGroupe = uneRepresentation.getGroupe().getNom();
        String nomLieu = uneRepresentation.getLieu().getNom();
        String nbPlacesVendues = String.valueOf(uneRepresentation.getPlacesVendues());
		
        System.out.println("AFFICHAGE REPRESENTATION N°2 : ");
        System.out.println(id+" - "+maDate+" - "+debut+" - "+fin+" - "+nomGroupe+" - "+nomLieu+" - "+nbPlacesVendues);
        }
    }
    

    
    


