package controleur;

import java.sql.Time;
import java.time.*;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;
import javax.swing.JOptionPane;
import modele.metier.Representation;


/**
 * Controleur principal (ou frontal) - un lien vers chaque contrôleur de base
 *
 * @author hcaillaud
 */
public class CtrlPrincipal {

    private CtrlLesRepresentations ctrlLesRepres = null;
    private CtrlUneRepresentation ctrlUneRepres = null;
    EntityManager em;
    EntityTransaction tx;
    
    public CtrlPrincipal(){
        em = Persistence.createEntityManagerFactory("FestivalBilleteriePU").createEntityManager();
    }

    /**
     * action par défaut action au démarrage de l'application
     */
    public void action() {
        if (ctrlLesRepres == null) {
            ctrlLesRepres = new CtrlLesRepresentations(this);
            ctrlLesRepres.getVue().setTitle("Représentation v 2018");
        }
        ctrlLesRepres.getVue().setEnabled(true);
        ctrlLesRepres.getVue().setVisible(true);
    }

    /**
     * Fin de l'application
     */
    public void listeQuitter() {
        try {
            em.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "CtrlPrincipal - fermeture connexion BD", JOptionPane.ERROR_MESSAGE);
        } finally {
            System.exit(0);
	}
    }
    
    /**
     * Quitte vueUneRepresentation et retourne sur vueLesRepresentations
     */
    public void represFichierQuitter() {
        if (ctrlLesRepres == null) {
            ctrlUneRepres = new CtrlUneRepresentation(this);
        }
        ctrlUneRepres.getVue().setVisible(false);
        ctrlLesRepres.getVue().setEnabled(true);
        ctrlLesRepres.getVue().setVisible(true);
    }  

    /**
     * Transition vueListeRepresentations / vueUneRepresentation
     * @param uneRepres
     */
    public void represListeAller(Representation uneRepres) {
        if (ctrlUneRepres == null) {
            ctrlUneRepres = new CtrlUneRepresentation(this);
        }
        
        LocalDate todayDate = LocalDate.now();
        LocalTime todayTime = LocalTime.now();
        
        LocalDate jourRepres = uneRepres.getJour().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        
        //Fin repres
        int hf = uneRepres.getFin().getHours();
        int mf = uneRepres.getFin().getMinutes();
        int sf = uneRepres.getFin().getSeconds();
        LocalTime finRepres = LocalTime.of(hf, mf, sf);
        
        //Début repres
        int hd = uneRepres.getDebut().getHours();
        int md = uneRepres.getDebut().getMinutes();
        int sd = uneRepres.getDebut().getSeconds();
        LocalTime debutRepres = LocalTime.of(hd, md, sd);
        
        int placesRestantes = uneRepres.getLieu().getCapacite() - uneRepres.getPlacesVendues();
        
        if(placesRestantes == 0) {
            JOptionPane.showMessageDialog(ctrlLesRepres.getVue(), "Il ne reste plus de place disponible.", "Avertissement", 0);
        }else if(DAYS.between(todayDate, jourRepres) < 0) {
            JOptionPane.showMessageDialog(ctrlLesRepres.getVue(), "La représentation est terminée.", "Avertissement", 0);
        } else if(DAYS.between(todayDate, jourRepres) == 0) {
            if(MINUTES.between(todayTime, finRepres) < 0){
                JOptionPane.showMessageDialog(ctrlLesRepres.getVue(), "La représentation est terminée.", "Avertissement", 0);
            } else if(MINUTES.between(todayTime, debutRepres) < -30) {
                JOptionPane.showMessageDialog(ctrlLesRepres.getVue(), "La représentation a commencée depuis plus de 30 minutes.", "Avertissement", 0);
            }
        }
        
        ctrlUneRepres.setUneRepres(uneRepres);
        
        ctrlLesRepres.getVue().setEnabled(false);
        ctrlLesRepres.getVue().setVisible(true);
        ctrlUneRepres.getVue().setEnabled(true);
        ctrlUneRepres.getVue().setVisible(true);
    }

    public EntityTransaction getTx() {
        return tx;
    }

    public void setTx(EntityTransaction tx) {
        this.tx = tx;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public CtrlLesRepresentations getCtrlLesRepres() {
        return ctrlLesRepres;
    }

    public void setCtrlLesRepres(CtrlLesRepresentations ctrlLesRepres) {
        this.ctrlLesRepres = ctrlLesRepres;
    }

    public CtrlUneRepresentation getCtrlUneRepres() {
        return ctrlUneRepres;
    }

    public void setCtrlUneRepres(CtrlUneRepresentation ctrlUneRepres) {
        this.ctrlUneRepres = ctrlUneRepres;
    }
    
}
