/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import modele.metier.Representation;
import vue.VueUneRepresentation;

/**
 * Contrôleur de la fenêtre VueUneRepresentation
 * @author nragazzi
 */
public class CtrlUneRepresentation extends ControleurGenerique implements WindowListener, ActionListener, MouseListener {

    private Representation uneRepres;
    private CtrlPrincipal ctrlPrincipal;
    
    public CtrlUneRepresentation(CtrlPrincipal ctrlPrincipal) {
        super(ctrlPrincipal);
        this.ctrlPrincipal = ctrlPrincipal;
        vue = new VueUneRepresentation();
		this.vue.setTitle("Details d'une representation");
        representationAfficher();
		((VueUneRepresentation)this.vue).getjButtonValider().addMouseListener(this);
        ((VueUneRepresentation)this.vue).addWindowListener(this);
    }
    
    public void representationQuitter() {
        this.getCtrlPrincipal().listeQuitter();
    }

    /**
     * Afficher remplit le modèle du composant jTableEquipier à
     * partir de la base de données
     */
    public final void representationAfficher() {
        
        String msg = ""; // message à afficher en cas d'erreur
        try {
            this.getCtrlPrincipal().action();
		} catch (Exception ex) {
            msg = "CtrlLesRepresentations - setLesDonnees() - " + ex.getMessage();
            JOptionPane.showMessageDialog(vue, msg, "Affichage des representations", JOptionPane.ERROR_MESSAGE);
        }
		
    }

    public Representation getUneRepres() {
        return uneRepres;
    }

    public void setUneRepres(Representation uneRepres) {
        this.uneRepres = uneRepres;
		this.setLesDonnees(uneRepres);
    }
	
	private void setLesDonnees(Representation uneRepres){
            ((VueUneRepresentation)this.vue).setjLabelNomGroupe(uneRepres.getGroupe().getNom());
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    String dateRepres = df.format(uneRepres.getJour());
            ((VueUneRepresentation)this.vue).setjLabelNomLieu(dateRepres);
            ((VueUneRepresentation)this.vue).setjLabelDate(uneRepres.getLieu().getNom());
            ((VueUneRepresentation)this.vue).setjLabelCap(Integer.toString(uneRepres.getLieu().getCapacite()));
                    DateFormat hf = new SimpleDateFormat("HH:mm");
                    String hDebut = hf.format(uneRepres.getDebut());
                    String hFin = hf.format(uneRepres.getFin());
            ((VueUneRepresentation)this.vue).setjLabelHD(hDebut);
            ((VueUneRepresentation)this.vue).setjLabelHF(hFin);
                    int placesRestante = uneRepres.getLieu().getCapacite() - uneRepres.getPlacesVendues();
            ((VueUneRepresentation)this.vue).setjLabelPlacesRestantes(Integer.toString(placesRestante));
            ((VueUneRepresentation)this.vue).enablejTextFieldPlacesVoulues();
            ((VueUneRepresentation)this.vue).showValidButton();
            if(placesRestante == 0){
                    ((VueUneRepresentation)this.vue).disablejTextFieldPlacesVoulues();
                    ((VueUneRepresentation)this.vue).hideValidButton();
            }else{
                LocalDate todayDate = LocalDate.now();
                LocalTime todayTime = LocalTime.now();

                LocalDate jourRepres = uneRepres.getJour().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                //Fin repres
                int hf2 = uneRepres.getFin().getHours();
                int mf = uneRepres.getFin().getMinutes();
                int sf = uneRepres.getFin().getSeconds();
                LocalTime finRepres = LocalTime.of(hf2, mf, sf);

                //Début repres
                int hd = uneRepres.getDebut().getHours();
                int md = uneRepres.getDebut().getMinutes();
                int sd = uneRepres.getDebut().getSeconds();
                LocalTime debutRepres = LocalTime.of(hd, md, sd);

                int placesRestantes = uneRepres.getLieu().getCapacite() - uneRepres.getPlacesVendues();

                if(DAYS.between(todayDate, jourRepres) < 0) {
                    ((VueUneRepresentation)this.vue).disablejTextFieldPlacesVoulues();
                    ((VueUneRepresentation)this.vue).hideValidButton();
                } else if(DAYS.between(todayDate, jourRepres) == 0) {
                    if(MINUTES.between(todayTime, finRepres) < 0){
                        ((VueUneRepresentation)this.vue).disablejTextFieldPlacesVoulues();
                        ((VueUneRepresentation)this.vue).hideValidButton();
                    } else if(MINUTES.between(todayTime, debutRepres) < -30) {
                        ((VueUneRepresentation)this.vue).disablejTextFieldPlacesVoulues();
                        ((VueUneRepresentation)this.vue).hideValidButton();
                    }
                }
            }
	}
	
	public void VendrePlaces(Representation uneRepres){
		int nbPlacesVoulues = ((VueUneRepresentation)this.vue).getPlaceVoulesValue();
			// System.out.println("boutton valider avec : " + nbPlacesVoulues + " places voulues");
			// System.out.println("Pour la représentation : " + uneRepres);
			// System.out.println("Places disponibles : " + (uneRepres.getLieu().getCapacite() - uneRepres.getPlacesVendues()));
			
			ctrlPrincipal.getEm().getTransaction().begin();
			if(uneRepres.getPlacesVendues() + nbPlacesVoulues > uneRepres.getLieu().getCapacite()){
				JOptionPane.showMessageDialog(((VueUneRepresentation)this.vue), "Il n'y a plus assez de places disponibles", "Avertissement", 0);
			}else{
				uneRepres.setPlacesVendues(uneRepres.getPlacesVendues() + nbPlacesVoulues);
			}
			
			// System.out.println("Places disponibles : " + (uneRepres.getLieu().getCapacite() - uneRepres.getPlacesVendues()));
			ctrlPrincipal.getEm().getTransaction().commit();
			setLesDonnees(uneRepres);
			this.ctrlPrincipal.getCtrlLesRepres().desRepresentationsAfficher();
	}
	
	@Override
    public void mouseClicked(MouseEvent e) {
        if( e.getSource().equals(((VueUneRepresentation)this.vue).getjButtonValider()) ){
			VendrePlaces(getUneRepres());
        }
    }
	
    @Override
    public void windowOpened(WindowEvent e) { }

    @Override
    public void windowClosing(WindowEvent e) {
        this.ctrlPrincipal.represFichierQuitter();
    }

    @Override
    public void windowClosed(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowDeactivated(WindowEvent e) { }

	@Override
	public void mousePressed(MouseEvent me) { }

	@Override
	public void mouseReleased(MouseEvent me) { }

	@Override
	public void mouseEntered(MouseEvent me) { }

	@Override
	public void mouseExited(MouseEvent me) { }

	@Override
	public void actionPerformed(ActionEvent ae) { }
    
}
