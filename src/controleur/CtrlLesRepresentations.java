/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.persistence.Query;
import javax.swing.table.DefaultTableModel;
import modele.metier.Representation;
import vue.VueListeRepresentations;

/** Contrôleur de la fenêtre VueListeRepresentations
 * @author hcaillaud
 */
public class CtrlLesRepresentations extends ControleurGenerique implements WindowListener, ActionListener, MouseListener {
    
    private List<Representation> lesRepresentations;
    
	
    public CtrlLesRepresentations(CtrlPrincipal ctrlPrincipal) {
        super(ctrlPrincipal);
        this.vue = new VueListeRepresentations();
		this.vue.setTitle("Liste des representations");
        ((VueListeRepresentations)vue).getjTableRepresentations().addMouseListener(this);
        desRepresentationsAfficher();
        ((VueListeRepresentations)this.vue).addWindowListener(this);
    }
    
    /**
     * DesRepresentationsAfficher remplit le modèle du composant jTableRepresentations à
     * partir de la base de données
     */
    public final void desRepresentationsAfficher() {
        Query query = ctrlPrincipal.getEm().createQuery("select r from Representation r");
        lesRepresentations = (List<Representation>) query.getResultList();
	
        ((VueListeRepresentations) vue).getModeleTableRepresentations().setRowCount(0);
        String[] titresColonnes = {"ID", "Date", "Heure début", "Heure fin", "Nom Groupe", "Nom Lieu", "Places vendues"};
        ((VueListeRepresentations) vue).getModeleTableRepresentations().setColumnIdentifiers(titresColonnes);
        String[] ligneDonnees = new String[7];
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat tf = new SimpleDateFormat("HH.mm.ss");
        for (Representation uneRepresentation : lesRepresentations) {
            ligneDonnees[0] = uneRepresentation.getId();
            
            String maDate = df.format(uneRepresentation.getJour());
            ligneDonnees[1] = maDate;
            
            String debut = tf.format(uneRepresentation.getDebut());
            ligneDonnees[2] = debut;
            
            String fin = tf.format(uneRepresentation.getFin());
            ligneDonnees[3] = fin;
            
            ligneDonnees[4] = uneRepresentation.getGroupe().getNom();
            ligneDonnees[5] = uneRepresentation.getLieu().getNom();
            
            ligneDonnees[6] = String.valueOf(uneRepresentation.getPlacesVendues());
			
            ((VueListeRepresentations) vue).getModeleTableRepresentations().addRow(ligneDonnees);
        }
    }
	
    private Representation getRepresFromModele(int numLigne) {
        DefaultTableModel dtm = ((VueListeRepresentations)this.vue).getModeleTableRepresentations();
        
        Query query = ctrlPrincipal.getEm().createQuery("select r from Representation r where r.id = :id");
        query.setParameter("id", (String) dtm.getValueAt(numLigne, 0));
        Representation repres = (Representation)query.getSingleResult();
        
        return repres;
    }
	
    @Override
    public void windowOpened(WindowEvent e) { }
    @Override
    public void windowClosing(WindowEvent e) { }
    @Override
    public void windowClosed(WindowEvent e) { }
    @Override
    public void windowIconified(WindowEvent e) { }
    @Override
    public void windowDeiconified(WindowEvent e) { }
    @Override
    public void windowActivated(WindowEvent e) { }
    @Override
    public void windowDeactivated(WindowEvent e) { }
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource().equals(((VueListeRepresentations)this.vue).getjTableRepresentations())){
            Point p = e.getPoint();
            int row = ((VueListeRepresentations)this.vue).getjTableRepresentations().rowAtPoint(p);
            Representation repres;
            repres = getRepresFromModele(row);
			ctrlPrincipal.represListeAller(repres);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) { }

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) { }

    @Override
    public void mouseExited(MouseEvent e) { }
    
}
