/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Lucas Sallé
 */
@Entity
@Table(name="Lieu")
public class Lieu implements Serializable{
    @Id
	@Column(name="IdLieu")
    private String id;
	@Column(name="Nom")
    private String nom;
	@Column(name="Adresse")
    private String adresse;
	@Column(name="CapAccueil")
    private int capacite;

	public Lieu() {
	}

    public Lieu(String id, String nom, String adresse, int capacite) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.capacite = capacite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    @Override
    public String toString() {
        return "Lieu{" + "id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", capacite=" + capacite + '}';
    }    
}
