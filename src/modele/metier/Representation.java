/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Lucas Sallé
 */
@Entity
@Table(name="Representation")
public class Representation implements Serializable{
    
    @Id
    @GeneratedValue
    @Column(name="IdRepresentation")
    private String id;
    @OneToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name="IdGroupe")
    private Groupe groupe;
    @OneToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name="IdLieu")
    private Lieu lieu;
    @Column(name="DateRep")
    @Temporal(TemporalType.DATE)
    private Date jour;
    @Column(name="HDebut")
    @Temporal(TemporalType.TIME)
    private Date debut;
    @Column(name="Hfin")
    @Temporal(TemporalType.TIME)
    private Date fin;
    @Column(name="placesVendues")
    private int placesVendues;

    public Representation() {
    }

    public Representation(String id, Groupe groupe, Lieu lieu, Date jour, Date debut, Date fin, int placesVendues) {
        this.id = id;
        this.groupe = groupe;
        this.lieu = lieu;
        this.jour = jour;
        this.debut = debut;
        this.fin = fin;
        this.placesVendues = placesVendues;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public Lieu getLieu() {
        return lieu;
    }

    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    public Date getJour() {
        return jour;
    }

    public void setJour(Date jour) {
        this.jour = jour;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public int getPlacesVendues() {
        return placesVendues;
    }

    public void setPlacesVendues(int placesVendues) {
        this.placesVendues = placesVendues;
    }

    @Override
    public String toString() {
        return "Representation{" + "id=" + id + ", groupe=" + groupe + ", lieu=" + lieu + ", jour=" + jour + ", debut=" + debut + ", fin=" + fin + ", placesVendues=" + placesVendues + '}';
    }
        
}
