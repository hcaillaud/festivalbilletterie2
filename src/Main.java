/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import controleur.*;

/**
 * @author lsalle
 */


public class Main {
    
    /**
     * @param args the command line argumentsl
     */
    public static void main(String[] args) {
        CtrlPrincipal leControleurPrincipal = new CtrlPrincipal();
         
        // instanciation contrôleur et vue sur la liste des représentations
        CtrlLesRepresentations leControleurLesRepresentations = new CtrlLesRepresentations(leControleurPrincipal);
        leControleurPrincipal.setCtrlLesRepres(leControleurLesRepresentations);
          
        // instanciation contrôleur et vue sur la liste des clients
         CtrlUneRepresentation leControleurUneRepres = new CtrlUneRepresentation(leControleurPrincipal);
         leControleurPrincipal.setCtrlUneRepres(leControleurUneRepres);
        
    }
}
